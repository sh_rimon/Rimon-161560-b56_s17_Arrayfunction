<?php
    $color = "Red";

    function callByValue($value){
        $value ="blue";
        echo "value = $value <br>";
    }

    callByValue($color); // called by value
    echo "color = $color <br>";

 echo "<hr>";

    function callByReference(&$value){
        $value ="blue";
        echo "value = $value <br>";
    }
    callByReference($color); // called by value
    echo "color = $color <br>";