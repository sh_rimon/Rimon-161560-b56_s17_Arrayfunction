<?php

    $input = array("green","read","blue","cyan");
    $reversed = array_reverse($input);

    echo "<pre>";
    print_r($input);
    print_r($reversed);
    echo "</pre>";

    echo"<br>";

    $input = array("A+","B+","AB+","O+");
    $preserved =array_reverse($input, true);

    echo "<pre>";
    print_r($input);
    print_r($preserved);

echo "<hr>";
// compact array function

    $x = 12;
    $y = 33;
    $radius = 15;
    $xyArray = array('x','y','radius');

    $circle = compact($xyArray);

    function drawCircle($circle){
        print_r($circle);
    }
    drawCircle($circle);

echo "<hr>";
// array unique array function

    $textArray = array(34,12,12,14,15,15,34,65,100,13,14);
    $outputArray = array_unique($textArray);
    print_r($outputArray);

echo "<hr>";
// Shift array function

    $stack = array("orange", "banana", "apple", "raspberry");
    $fruit = array_shift($stack);
    print_r($stack);

echo "<hr>";
// Unshift array function

    $stack = array("orange", "banana", "apple", "raspberry");
    echo array_unshift($stack);
    print_r($stack);

echo "<hr>";
// Each array function

    $stackArray = array("o"=>"orange", "b"=>"banana", "a"=>"apple", "r"=>"raspberry");
    print_r($stackArray); // return key value of the current  cousor and adavance the
    echo"<br>";

    $kvPair = each($stackArray);
    print_r($kvPair);
    echo"<br>";

echo "<hr>";
// Current  array function

    $kvPair = current($stackArray); // just return the value of current element
    print_r($kvPair);
    echo"<br>";

echo "<hr>";
// Next  array function

    $kvPair = next($stackArray); // just return the value of next element
    print_r($kvPair);
    echo"<br>";

echo "<hr>";
// pre array function

    $kvPair = prev($stackArray); // just return the value of next element
    print_r($kvPair);
    echo"<br>";

echo "<hr>";
// end array function

    $kvPair = end($stackArray); // just return the value of next element
    print_r($kvPair);
    echo"<br>";

echo "<hr>";
// reset array function

    $kvPair = reset($stackArray); // just return the value of next element
    print_r($kvPair);
    echo"<br>";

echo "<hr>";
// key exit array function

    $stackArray = array("o"=>"orange", "b"=>"banana", "a"=>"apple", "r"=>"raspberry");
    if (array_key_exists("x",$stackArray));
        echo $stackArray['x'];